#ifndef MENU_H
#define MENU_H

#include <iostream>
#include <string>
#include <fstream>

#include "Supermercado.hpp"

/**
 *@brief Programa responsável por gerar um menu de opções na qual o usuário pode fazer adição de clientes, listar produtos, etc.
 *@author Pedro Victor da Fonseca
 *@date 18/11/2019
 */

using namespace std;


bool iniciar(string& arquivoClientes, string& arquivoProdutos);
bool arquivoExiste(string arquivo);

void menuPrincipal(Supermercado &supermercado);
void menuCliente(Supermercado &supermercado);
void menuEstoque(Supermercado &supermercado);

void alertaAdeus(); // Mensagem de saída do programa

#endif