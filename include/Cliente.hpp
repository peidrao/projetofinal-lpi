#ifndef CLIENTE_H
#define CLIENTE_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream> // Biblioteca para usar o stringstream  (associação de string com um fluxo de arquivo)

#include "Data.hpp"

using namespace std;

/**
 * 
 *@file Cliente.hpp
 *@version 1.0
 *@date 18/11/2019
 *@author Pedro Victor da Fonseca
 *@brief Métodos responsáveis pelo cliente do supermercado
 */

class Cliente {
public:
    /// Construtores;
    Cliente() {};
    Cliente(ifstream &input);
    Cliente( unsigned int id, string nome, float valorGasto, Data(data));

    /// Getters
    string getNome() const;
    int getId() const;
    float getValorFasto() const;
    Data getdataInscricao() ;

    /// Setters
    void setNome(string nome);
    void salvar(ofstream & output);


    friend ostream& operator << (ostream& output, const Cliente& cliente);      /// Operator para imprimir o cadastro do cliente na tela.
    friend bool operator <(const Cliente& clienteA, const Cliente& clienteB); 
    friend bool operator==(const Cliente& clienteA, string nome);
    friend bool operator==(const Cliente& clienteA,unsigned int id);
    friend bool operator==(const Cliente& clienteA, const Cliente& clienteB);

private:
    unsigned int id_;       /// ID do cliente
    string nome_;           /// Nome do cliente
    Data dataInscricao_;    /// Data em que o cliente foi adicionado ao sistema do Supermercado
    float valorGasto_;      /// Valor que o cliente gastou em suas compras.

};

#endif // !CLIENTE_H