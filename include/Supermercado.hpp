#ifndef SUPERMERCADO_H
#define SUPERMERCADO_H

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <algorithm>
#include <set>
#include <vector>
#include <cstdlib>

#include "Cliente.hpp"
#include "Estoque.hpp"


using namespace std;

class Supermercado
{
public:
	Supermercado(string clientesArquivo, string produtosArquivo);
	~Supermercado();

	void cadastrarClientes(); 
	void editarCliente(); 
	void listarClientes(); 
	void removerCliente(); 
	void buscarClientesNome();

	void carregarClientes();
	void carregarEstoque(); 
	void salvarAlteracoes(); 

	void cadastrarProduto();
	void listarProdutos();
	void removerProduto();

	void veficiarIdExistentes(int &id, string type, string operation);
	void verificarID(int &id, string type); 
	void verificarINome(string &nome, string type);

	template<typename T>
	Cliente* getCliente(T t); 

	template<typename T>
	Estoque* getEstoque(T t);

private:
	string clientesArquivo_; // Ficheiro de clientes;
	string produtosArquivo_; // Ficheiro de produtos; 
	
	bool clienteAlterado_ = false; // Fica true quando alguma alteração acontece no cliente;
	bool produtoAlterado_ = false;
	int clienteMaxID_; // Onde é armazenado o identificador máximo que o cliente armazena;
	int produtoMaxID_;

	vector<Cliente> clientes_; // Vetor que armazena informações dos clientes;
	map<string, int> clienteId_; // Buscar cliente correspondete ao seu id;

	vector<Estoque> produtos_;
	map<string, int> produtoId_;

};

#endif