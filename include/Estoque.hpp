#ifndef ESTOQUE_H
#define ESTOQUE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

/**
 * 
 *@file Estoque.hpp
 *@version 1.0
 *@date 16/11/2019
 *@author Pedro Victor da Fonseca
 *@brief Métodos responsáveis por gerirem o estoque
 */

class Estoque {
public:
	Estoque(ifstream& input);
	Estoque(int id, string nomeProduto, int qauntidadeProduto, float precoProduto);
	~Estoque();

	int getId() const;
	
	int getQuantidadeProduto() const;
	void setQuantidadeProduto(int quantidadeProduto);

	string getNomeProduto() const;
	void setNomeProduto(string nomeProduto);

	float getPrecoProduto() const;
	void setPrecoProduto(float precoProduto);

	void salvar(ofstream & output);

	friend ostream& operator << (ostream& output, const Estoque& estoque);
    friend bool operator <(const Estoque& estoqueA, const Estoque& estoqueB); 
    friend bool operator==(const Estoque& estoque, string nomeProduto);
    friend bool operator==(const Estoque& estoque, int id);

private:
	int id_;
	int quantidadeProduto_;
	string nomeProduto_;
	float precoProduto_;

};


#endif