#ifndef DATA_H
#define DATA_H

#include <string>
#include <string>
#include <istream>
#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

/**
 * 
 *@file Data.hpp
 *@version 1.0
 *@date 18/11/2019
 *@author Pedro Victor da Fonseca
 *@brief Classe que faz a representação de datas.
 */

class Data {
public:
	Data(); 			/// Construtor padrão.
	Data(string data);	/// Construtor.

	int getDia() const;		// Retorna o dia.
	int getMes() const;		// Retorna o mes.
	int getAno() const;		// Retorna o ano.
	
	void salvar(ofstream& out) const;
	friend ostream& operator<<(ostream& out, const Data & data );

private:
	int dia_;
	int mes_;
	int ano_;
};

#endif