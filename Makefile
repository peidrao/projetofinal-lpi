CC=g++

CFLAGS= -Wall -pedantic -std=c++11 -I./include

BIN_PATH = ./bin
BUILD_PATH = ./build
INCLUDE_PATH = ./include
#LIB_PATH = ./lib
SRC_PATH = ./src

CLEAN = clean

debug: CFLAGS += -O0 -g

Run: ./build/menu.o ./build/data.o ./build/supermercado.o ./build/cliente.o ./build/estoque.o ./build/main.o
	$(CC) $(CFLAGS) -o ./bin/$@ $^

./build/menu.o: ./src/menu.cpp ./include/Menu.hpp
	$(CC) -c $(CFLAGS) -o $@ $<

./build/data.o: ./src/data.cpp ./include/Data.hpp
	$(CC) -c $(CFLAGS) -o $@ $<

./build/supermercado.o: ./src/supermercado.cpp ./include/Supermercado.hpp
	$(CC) -c $(CFLAGS) -o $@ $<

./build/cliente.o: ./src/cliente.cpp ./include/Cliente.hpp
	$(CC) -c $(CFLAGS) -o $@ $<

./build/estoque.o: ./src/estoque.cpp ./include/Estoque.hpp
	$(CC) -c $(CFLAGS) -o $@ $<

#main.o
./build/main.o: ./src/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $< -g

purge: $(CLEAN)
		@$(RM) -f $(BIN_NAME)

$(CLEAN):
	@$(RM) -f $(BUILD_NAME)/*
	@$(RM) -f $(BIN_PATH)/*
	@$(RM) -f $(LIB_PATH)/*
