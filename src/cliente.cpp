#include "Cliente.hpp"


/**
 *@file cliente.cpp
 *@brief Construtor que faz a criação de um novo objeto do tipo cliente.
 *@author Pedro Victor da Fonseca
 *@param input 
 */

Cliente::Cliente(ifstream& input) {
    string linha, lixo, stringData;
    getline(input, linha);
    stringstream iss(linha);

    iss >> id_;
    getline(iss, lixo, ';');

    getline(iss >> ws, nome_, ';' );
    nome_.pop_back();
    iss >> valorGasto_;

    getline(iss, lixo, ';');
    
    getline(iss >> ws, stringData, ';');
    dataInscricao_ = Data(stringData);
    
}

/// Construtor que faz o setter dos dados do cliente.
Cliente::Cliente(unsigned int id, string nome, float valorGasto, Data data) {
    this->id_ = id;
    this->nome_ = nome;
    this->valorGasto_ = valorGasto; 
    this->dataInscricao_ = data;
}

/// Método get que realiza o retorno do nome do usuário.
string Cliente::getNome() const {
    return nome_;
}

/// Método get, responsável por retornar o ID do cliente.
int Cliente::getId() const {
    return id_;
}

float Cliente::getValorFasto() const {
    return valorGasto_;
}

Data Cliente::getdataInscricao() {
    return dataInscricao_;
}

void Cliente::setNome(string nome) {
    this->nome_ = nome;
}

void Cliente::salvar(ofstream& output) {
    string linha;
    stringstream ss;
    ss << id_ << " ; " << nome_ <<  " ; " << valorGasto_  << " ; " << dataInscricao_;
    linha = ss.str();

    output << linha;
}


ostream& operator<< (ostream& output, const Cliente& cliente) {
    output << " *** Cliente ID. " << cliente.id_ << " ***" << endl;
    output << "Nome: " << cliente.nome_ << endl;
    output << "Valor Gasto: R$" << cliente.valorGasto_ << endl;
    output << "Data: " << cliente.dataInscricao_ << endl;
    return output;
}


bool operator<(const Cliente &clientA, const Cliente &clientB) {
    return (clientA.nome_ < clientB.nome_);
}

bool operator==(const Cliente& cliente, string nome) {
    return (cliente.nome_ == nome);
}


bool operator==(const Cliente& cliente, unsigned int id) {
    return (cliente.id_ == id);
}

bool operator == (const Cliente &clientA, const Cliente &clientB) {
    return (clientA.getId() == clientB.getId());
}