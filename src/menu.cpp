#include "Menu.hpp"

bool arquivoExiste(string nomeArquivo) {
	bool existe = false;
	ifstream inputFile;
	inputFile.open(nomeArquivo);
	if (inputFile.is_open())
	{
		existe = true;
		inputFile.close();
	}
	return existe;
}

/**
 *@brief Função responsável pela validação do arquivos que serão usados.
 *@author Pedro Victor da Fonseca
 *@date 18/11/2019
 *@param arquivoCliente 
 *@param arquivoProdutos 
 *@return true 
 *@return false 
 */

bool iniciar(string& arquivoCliente, string& arquivoProdutos) {
	bool validarArquivoCliente = false;
	bool validarArquivoProduto = false;

	system("clear");
	cout << "\t|||||||||||||||||||||||||||||||||||||||||||||||||||||\n"
		 << "\t|                                                   |\n"
		 << "\t|              B E M  V I N D O  A O                |\n"
		 << "\t|        S I S T E M A  D E  C O N T R O L E        |\n"
		 << "\t|                                                   |\n"
		 << "\t|||||||||||||||||||||||||||||||||||||||||||||||||||||\n"
		 << "\t|                                                    \n"
		 << "\t|                                                    \n"
		 << "\t|    D I G I T E  O  N O M E  D O  A R Q U I V O     \n"
		 << "\t|                                                    \n";

		 //Arquivo do cliente
	do {
		cout << "\t|Arquivo dos clientes ------->>>>> ";
		cin >> arquivoCliente;
		if (arquivoExiste(arquivoCliente))
			validarArquivoCliente = true;
		else 
		{	
			cout << "Arquivo não existe!\n"
			     << "Digite mais uma vez o nome do arquivo: \n";
		}
	} while(!arquivoExiste(arquivoCliente));

	// Arquivo Estoque
	do {
		cout << "\t|Arquivo dos produtos ------->>>>> ";
		cin >> arquivoProdutos;
		if(arquivoExiste(arquivoProdutos))
			validarArquivoProduto = true;
		else 
		{
			cout << "Arquivo não existe!\n"
			     << "Digite mais uma vez o nome do arquivo: ";
		}
	}while(!arquivoExiste(arquivoProdutos));
	cout << "leitura aconteceu!\n" << endl;
	// Validação Final
	if(validarArquivoCliente&&validarArquivoProduto)
		return true;
	else
		return false;
}

void menuPrincipal (Supermercado& supermercado) {
	int opc;
	system("clear");
	cout << endl;

	do
	{	
		do
		{
			cout << "*=====================================*\n"
		 	  << "|       S U P E R M E R C A D O       |\n"
		 	  << "|                D O                  |\n"
		 	  << "|       P R O F.  U M B E R T O       |\n"
		 	  << "*=====================================*\n"
		 	  << "|      M E N U  P R I N C I P A L     |\n"
		 	  << "*-------------------------------------*\n"
		 	  << "| (1) - Menu do Cliente               |\n"
		 	  << "| (2) - Menu do Estoque               |\n"
		 	  << "| (0) - Sair                          |\n"
		 	  << "|                                     |\n"
		 	  << "|*************************************|\n"
		 	  << "|                 SELECIONE UMA OPÇÃO |\n"
		 	  << "*=====================================*\n"
		 	  << "|\n"
		 	  << "|\n"
		 	  << "|--------->>> ";

		 	cin >> opc;
		} while(opc != 1 && opc != 0 && opc != 2 );

		switch(opc) 
		{
			case 0:
				alertaAdeus();
				exit(1);
				break;
			case 1:
				system("clear");
				menuCliente(supermercado);
				break;
			case 2:
				system("clear");
				menuEstoque(supermercado);
				break;
			default:
				break;
		}
	} while(opc!= 0);
}

void menuCliente(Supermercado& supermercado) {
	int opc;
	
	do
	{
		cout << "*=====================================*\n"
	 	  << "|       S U P E R M E R C A D O       |\n"
	 	  << "|                D O                  |\n"
	 	  << "|       P R O F.  U M B E R T O       |\n"
	 	  << "*=====================================*\n"
	 	  << "|      M E N U  D O  C L I E N T E    |\n"
	 	  << "*-------------------------------------*\n"
	 	  << "| (1) - Adicionar cliente             |\n"
	 	  << "| (2) - Editar clientes               |\n"
	 	  << "| (3) - Listar clientes               |\n"
	 	  << "| (4) - Remover clientes              |\n"
	 	  << "| (5) - Procurar clientes pelo nome   |\n"
	 	  << "| (0) - Voltar p/ menu principal      |\n"
	 	  << "|                                     |\n"
	 	  << "|*************************************|\n"
	 	  << "|                 SELECIONE UMA OPÇÃO |\n"
	 	  << "*=====================================*\n"
	 	  << "|\n"
	 	  << "|\n"
	 	  << "|--------->>> ";

	 	  cin >> opc;

			} while(opc != 1 && opc != 0 && opc != 2 && opc != 3 && opc != 4 && opc != 5);

		 switch (opc)
		 {
		 	case 0:
				menuPrincipal(supermercado);
				break;
		 	case 1:
		 		supermercado.cadastrarClientes();
		 		break;
		 	case 2:
		 		supermercado.editarCliente();
		 		break;
		 	case 3: 
		 		supermercado.listarClientes();
		 		break;
		 	case 4: 
		 		supermercado.removerCliente();
		 		break;
		 	case 5:
		 		supermercado.buscarClientesNome();
		 		break; 

		 	default:
				break;
		 }
	supermercado.salvarAlteracoes();
}


void menuEstoque(Supermercado& supermercado) {
	int opc;
	do
	{
		cout << "*=====================================*\n"
	 	  << "|       S U P E R M E R C A D O       |\n"
	 	  << "|                D O                  |\n"
	 	  << "|       P R O F.  U M B E R T O       |\n"
	 	  << "*=====================================*\n"
	 	  << "|      M E N U  D O  E S T O Q U E    |\n"
	 	  << "*-------------------------------------*\n"
	 	  << "| (1) - Adicionar produto             |\n"
	 	  << "| (2) - Listar produtos               |\n"
	 	  << "| (3) - Remover produtos              |\n"
	 	  << "| (0) - Voltar p/ menu principal      |\n"
	 	  << "|                                     |\n"
	 	  << "|*************************************|\n"
	 	  << "|                 SELECIONE UMA OPÇÃO |\n"
	 	  << "*=====================================*\n"
	 	  << "|\n"
	 	  << "|\n"
	 	  << "|--------->>> ";

	 	  cin >> opc;

			} while(opc != 1 && opc != 2 && opc != 3 && opc != 0);

		 switch (opc)
		 {
		 	case 0:
				menuPrincipal(supermercado);
				break;
		 	case 1:
		 		supermercado.cadastrarProduto();
		 		break;
		 	case 2:
		 		supermercado.listarProdutos();
		 		break;
		 	case 3: 
		 		supermercado.removerProduto();
		 		break;	
		 	default:
				break;
		 }
	supermercado.salvarAlteracoes();
}

void alertaAdeus() {
	system("clear");
	cout << "\t|||||||||||||||||||||||||||||||||||||||||||||||||||||\n"
		 << "\t|                                                   |\n"
		 << "\t|             A T É  A  P R Ó X I M A               |\n"
		 << "\t|                                                   |\n"
		 << "\t|||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n";
	
}