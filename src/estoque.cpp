#include "Estoque.hpp"

Estoque::Estoque(ifstream & input) {
	string linha, lixo;
	getline(input, linha);

	stringstream iss(linha);

	iss >> id_;
	getline(iss, lixo, ';');
	getline(iss >> ws, nomeProduto_, ';');
	nomeProduto_.pop_back();
	iss >> quantidadeProduto_;
	getline(iss, lixo, ';');
	iss >> precoProduto_;
}

Estoque::Estoque(int id, string nomeProduto, int quantidadeProduto, float precoProduto) {
	this->id_ = id;
	this->nomeProduto_ = nomeProduto;
	this->quantidadeProduto_ = quantidadeProduto;
	this->precoProduto_ = precoProduto;
}

Estoque::~Estoque() = default;

int Estoque::getId() const {
	return id_;
}

int Estoque::getQuantidadeProduto() const {
	return quantidadeProduto_;
}

void Estoque::setQuantidadeProduto(int quantidadeProduto) {
	quantidadeProduto_ = quantidadeProduto;
}

string Estoque::getNomeProduto() const {
	return nomeProduto_;
}

void Estoque::setNomeProduto(string nomeProduto) {
	nomeProduto_ = nomeProduto;
}

float Estoque::getPrecoProduto() const {
	return precoProduto_;
}

void Estoque::setPrecoProduto(float precoProduto) {
	precoProduto_ = precoProduto;
}

void Estoque::salvar(ofstream& output) {
	string linha;
	stringstream ss;

	ss << id_ << " ; " << nomeProduto_ << " ; " << quantidadeProduto_ << " ; " << precoProduto_ ;
	linha = ss.str();
	output << linha;
}

ostream& operator << (ostream& output, const Estoque& estoque) {
	output << "*** Produto " << estoque.id_ << " ***" << endl
	       << "Nome do produto: " << estoque.nomeProduto_ << endl
	       << "Quantidade do produto: " << estoque.quantidadeProduto_ << endl
	       << "Preço do produto: " << estoque.precoProduto_ << endl;
	       return output;
}

bool operator <(const Estoque& estoqueA, const Estoque& estoqueB) {
	return (estoqueA.nomeProduto_ < estoqueB.nomeProduto_);
}

bool operator==(const Estoque& estoque, string nomeProduto) {
	return (estoque.nomeProduto_ == nomeProduto);
}

bool operator==(const Estoque& estoque, int id) {
	return (estoque.id_ == id);
}


