#include <iostream>
#include <string>

#include "Menu.hpp"
#include "Supermercado.hpp"

int main() {
    string arquivoCliente, arquivoProduto;
    if (!iniciar(arquivoCliente, arquivoProduto))
    {
    	return (-1);
    }

    Supermercado supermercado(arquivoCliente, arquivoProduto);
	menuPrincipal(supermercado);

}