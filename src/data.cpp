#include "Data.hpp"

Data::Data() = default;

Data::Data(string data) {
	stringstream linha(data);
	linha >> dia_; linha.ignore();
	linha >> mes_; linha.ignore();
	linha >> ano_; linha.ignore();
}


int Data::getDia() const {
	return dia_;
}

int Data::getMes() const {
	return mes_;
}

int Data::getAno() const {
	return ano_;
}

/// Faz a escrita da data que o cliente foi cadastrado no supermercado, no arquivo dedicado a ele
void Data::salvar(ofstream &out) const {
	out << dia_ << '/' << mes_ << '/' << ano_;
}

/// Faz a impressão da data quando um novo cliente é adicionado ao sistema, ou também, quando listamos dados relacionados a eles.
ostream& operator<<(ostream& out, const Data& data) {
	out << data.getDia() << '/' << data.getMes() << '/' << data.getAno();
	return out;
}
