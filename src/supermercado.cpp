#include "Supermercado.hpp"

//CONSTRUTOR DO SUPERMERCADO;

Supermercado::Supermercado(string clientesArquivo, string produtosArquivo) {
	this->clientesArquivo_ = clientesArquivo;
	this->produtosArquivo_ = produtosArquivo;
	carregarClientes();
	carregarEstoque();
}

// DESTRUTOR DO SUPERMERCADO
Supermercado::~Supermercado() = default;


void Supermercado::carregarClientes() {
	clienteMaxID_ = 0;
	int i = 0;
	string numClientes;

	ifstream arquivoPrincipal;
	arquivoPrincipal.open(clientesArquivo_);
	if (arquivoPrincipal.fail())
	{
		cerr << "Erro ao abrir arquivo!";
		exit(1);
	}
	else
	{
		getline(arquivoPrincipal, numClientes);
		while (!arquivoPrincipal.eof())
		{
			Cliente cliente(arquivoPrincipal);
			
			clientes_.push_back(cliente);
			clienteId_.insert(std::pair<string, int>(cliente.getNome(), i++));

			if(cliente.getId() > clienteMaxID_)
				clienteMaxID_ = cliente.getId();
		}
		arquivoPrincipal.close();
	}
}


void Supermercado::carregarEstoque() {
	produtoMaxID_ = 0;
	int i = 0;
	string numProdutos;

	ifstream arquivoPrincipal;
	arquivoPrincipal.open(produtosArquivo_);

	if (arquivoPrincipal.fail()) 
	{
		cerr << " Erro ao abrir arquivo de estoque!\n";
		exit(1);
	}
	else 
	{
		getline(arquivoPrincipal, numProdutos);
		while(!arquivoPrincipal.eof()) {
		    Estoque estoque(arquivoPrincipal);
		    produtos_.push_back(estoque);

		    produtoId_.insert(std::pair<string, int>(estoque.getNomeProduto(), i++));

		    if (estoque.getId() > produtoMaxID_)
		    {	
		    	produtoMaxID_ = estoque.getId();
		    }
		}
		
	}
	arquivoPrincipal.close();

	cout << "arquivo carregado!\n";
}

void Supermercado::cadastrarClientes() {
	string nome;
	string data;
	float valorGasto;
	int i = clientes_.size();
	int id = ++clienteMaxID_;

	cin.ignore();
	cout << "Cadastre o novo cliente: \n\n";
	cout << "Nome: " ;
	getline(cin, nome);
	cout << "Valor gasto pelo cliente: ";
	cin >> valorGasto;
	cin.ignore();
	cout << "Data: ";
	getline(cin, data);
	
	Cliente cliente(id, nome, valorGasto, Data(data));
	clientes_.push_back(cliente);
	clienteId_.insert(std::pair<string, int>(nome, ++i));
	clienteAlterado_ = true;

	cout << "Cliente cadastrado no sistema\n";
	cout << cliente;
}

void Supermercado::editarCliente() {
	int id;
	string novoNome;

	cin.ignore(1000, '\n');

	if (clientes_.empty()) 
	{
		cout << "Não existem clientes cadastrados!" << endl;

	}
	else 
	{
		cout << "Digite o ID do cliente que irá modificar: ";
		cin >> id;
		cin.ignore(1000, '\n');
		verificarID(id, "cliente");

		while(getCliente(id) == nullptr) 
		{
			veficiarIdExistentes(id, "cliente", "modify");		
		}
		
		Cliente* clientePonteiro = getCliente(id);
		cout << *clientePonteiro;

		cout << "Digite um novo nome para o cliente: ";
		getline(cin, novoNome);

		(*clientePonteiro).setNome(novoNome);

		clienteAlterado_ = true;

		cout << "Cliente modificado!";
	}
}

void Supermercado::buscarClientesNome() {
	string nome;
	if (clientes_.empty())
	{
		cout << "Não existem clientes cadastrados!" << endl;
	} 
	else
	{
		cin.ignore(1000, '\n');
		cout << "Digite o nome do cliente: ";
		getline(cin, nome);
		
		while(getCliente(nome) == nullptr)
			verificarINome(nome, "cliente");

		Cliente *clientePonteiro = getCliente(nome);

		cout << "Cliente encontrado!" << endl;
		cout << (*clientePonteiro);
	}
}

void Supermercado::listarClientes() {
	system("clear");
	if (clientes_.empty())
		cout << "Nenhum cliente!";
	else {
		for (const auto& c:clientes_)
		{	
			cout << c;
		}
	cout << endl;
	}
}

void Supermercado::removerCliente() {
	int id;
	char ch; 

	if (clientes_.empty())
	{
		cout << "Não existe nenhum cliente!";
	}
	else {
		listarClientes();
		cout << "Digite o ID do cliente que deseja remover: ";
		cin >> id;
		cin.ignore(1000, '\n');

		verificarID(id, "cliente");

		while(getCliente(id) == nullptr) 
		{
		    veficiarIdExistentes(id, "cliente", "remove");
		}

		Cliente* clientePonteiro = getCliente(id);

		int i = clienteId_[((*clientePonteiro).getNome())];
		cout << (*clientePonteiro);	
		cout << "Deseja remover o cliente? [s/n]: ";
		cin >> ch;
		cin.ignore(1000, '\n');

		if (toupper(ch) == 'S')
		{
			clientes_.erase(clientes_.begin() + i);
			clienteId_.erase((*clientePonteiro).getNome());
			cout << "Cliente Removido!";
		}
		clienteAlterado_ = true;
	}
}

void Supermercado::verificarID(int &id, string type) {
	while(cin.fail()) 
	{
		cout << "Digite um ID válido" << type << " ID!" << endl;
		cin >> id;
		cin.ignore(1000, '\n');
	}
}


void Supermercado::verificarINome(string &nome, string type) {
	cout << type << " Não está cadastrado(a) no nosso sistema!" << endl
	     << "Por favor dgite um nome válido: ";
	     getline(cin, nome);  
}

void Supermercado::veficiarIdExistentes(int &id, string type, string operation) {
	cout << "O ID digitado não existe!" << endl 
	     << "Digite um ID válido: ";
	cin >> id;

	while(cin.fail()) 
	{
	    cin.clear();
	    cin.ignore(1000, '\n');
	    cout << "Digite um ID válido: ";
	    cin >> id;
		cin.ignore(1000, '\n');
	}
}

template <typename T>
Cliente* Supermercado::getCliente(T t) {
	for (auto& c : clientes_)
	{
		if(c == t) {
			Cliente* clientePonteiro = &c;
			return clientePonteiro;
		}
	}
	return nullptr;
}

template <typename T >
Estoque* Supermercado::getEstoque(T t) {
	for(auto& p : produtos_) {
		if(p == t) {
			Estoque* estoquePonteiro = &p;
			return estoquePonteiro;
		}
	}
	return nullptr;
}

void Supermercado::cadastrarProduto() {
	int id = ++produtoMaxID_;
	string nome;
	float precoProduto = 0;
	int quantidade = 0;
	cin.ignore();
	cout << "Adicionar Produto" << endl;

	cout << "nome do produto: ";
	getline(cin, nome);
	cout << "Quantas unidades deseja adicionar: ";
	cin >> quantidade;
	cout << "Preço do produto: ";
	cin >> precoProduto;
	cin.ignore();

	Estoque estoque(id, nome, quantidade, precoProduto);
	produtos_.push_back(estoque);

	produtoId_.insert(std::pair<string, int>(nome, id));

	cout << "produto Adicionado com sucesso\n";
	produtoAlterado_ = true;
}

void Supermercado::listarProdutos() {
	if (produtos_.empty()) 
		cout << "Não existem produtos em estoque!" << endl;
	else 
	{
		for(const auto& p : produtos_) {
			cout << p;
		}
	}
}

void Supermercado::removerProduto() {
	int id;
	char opc;
	
	if(produtos_.empty())
		cout << "Não existe nenhum produto no estoque!" << endl;
	else 
	{
		listarProdutos();
		cout << "Digite o ID do produto que irá remover: ";
		cin >> id;
		cin.ignore(1000, '\n');

		verificarID(id, "estoque");

		while((getEstoque(id)) == nullptr) {
			veficiarIdExistentes(id, "estoque", "remove");
		}
		    Estoque* estoquePonteiro = getEstoque(id);
		    int i =  produtoId_[((*estoquePonteiro).getNomeProduto())];
		    cout << "Deseja realmente remover esse produto? [y/n]: ";
		    cin >> opc;
		    cin.ignore(1000, '\n');

		    if (toupper(opc) == 'Y')
		    {
		    	produtos_.erase(produtos_.begin() + i);
		    	produtoId_.erase((*estoquePonteiro).getNomeProduto());

		    	cout << "Produto removido com sucesso!" << endl;

		    	produtoAlterado_ = true;
		    }
	}	
}


void Supermercado::salvarAlteracoes() {
	if (clienteAlterado_)
	{
		ofstream outputFile;
		outputFile.open(clientesArquivo_);
		string linha;

		if(outputFile.fail()) 
		{
			cerr << "Erro ao abrir arquivo!\n";
			exit(1);
		}
		else 
		{
			outputFile << clientes_.size() << endl;

			for (auto& c : clientes_)
			{
				if(c.getId() != clientes_.back().getId()) {
					c.salvar(outputFile);
					outputFile << endl;
				}
				else
					c.salvar(outputFile);
			}
		}
	outputFile.close();
	}
	// Salvar Produtos
		if (produtoAlterado_)
	{
		ofstream outputFile;
		outputFile.open(produtosArquivo_);
		string linha;

		if(outputFile.fail()) 
		{
			cerr << "Erro ao abrir arquivo!\n";
			exit(1);
		}
		else 
		{
			outputFile << produtos_.size() << endl;

			for (auto& p : produtos_)
			{
				if(p.getId() != produtos_.back().getId()) {
					p.salvar(outputFile);
					outputFile << endl;
				}
				else
					p.salvar(outputFile);
			}
		}
	outputFile.close();
	}

}