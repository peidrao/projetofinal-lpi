<h1>Projeto Final: Linguagem de Programação I</h1>
    <h2>Controle de Estoque</h2>
    <ul>
        <li><h3>Problema Tratado</h3></li>
        <p>O problema que foi tratado durante toda essa terceira unidade foi o controle de estoque de um supermercado. Como fazer o registro das compras que são realizadas e como cadastrar novos funcionários entre outras funcionalidades que um supermercado possui. Dessa forma, implementamos um sistema que fizesse a manipulação não só de um estoque, mas como também da gestão de clientes (pois um grande supermercado precisa pensar naqueles que geram o lucro). Tudo isso de uma forma simples e bem elaborada para o usuário que usar o software.</p>
        <li><h3>Containers</h3></li>
        <p>Os containers que foram utilizados para esse projeto foram principalmente o de vetores, para guardar informações dos clientes como também dos produtos que estão dentro do estoque.</p>   
        <li><h3>Hierarquia de classes</h3></li>
        <p>A classe que temos como a principal é a <b>Supermercado</b>, é nela que fazemos todas as manipulações (abertura de arquivo, escrita de dados, colheita de informações do cliente e produto). As classes <b>Estoque</b> e <b>Cliente</b>, são outras classes bastante importantes, são nelas onde existem os dados que usaremos para cadastro dos novos clientes, e produtos. Uma observação importante sobre essas duas classes é que elas são agregadas a classe principal Supermercado (agregadas pois não possuem nenhuma herança com supermercado). Por fim ainda podemos contar com outra classe, que é a Data. É a responsável pela data que o cliente foi cadastrado no supermercado. Poderia ser usada para outras coisas, como data em que o produto foi colocado no estoque, ou na data em que determinado funcionário, mas lamentavelmente não houve tempo o suficiente para fazer tais implementações. </p>       
        <li><h3>Tratamento de Exceções</h3></li>
        <p>Os principais tratamento de exceções foram relacionadas na escrita que o usuário realizou, por exemplo, se o mesmo digitar o nome errado quando realiza uma pesquisa relacionada aos nomes dos clientes cadastrados no sistema e ele por sua vez digitar errado, será mostrado uma mensagem informado que o nome não está no sistema. A mesma coisa para um ID. Se o usuário digitar um id que não está no sistema, então será retornada uma mensagem informando que o id em questão não está no sistema. Logo abaixo estão os métodos que fazem esse tratamento:</p>
        <ul> 
            <li><b>void</b> veficiarIdExistentes(int &id, string type, string operation)</li>
            <li><b>void</b> verificarINome(string &nome, string type)</li>
            <li><b>void</b> verificarID(int &id, string type)</li>
            <li><b>bool</b> arquivoExiste(string nomeArquivo)</li>
        </ul>
        <li><h3>Funcionalidades Implementadas</h3></li>
            <p>As principais funcionalidades aplicadas foram baseadas no controle de estoque de um supermercado, contudo, podemos perceber no projeto em questão que existem mais funcionalidades dedicadas ao cliente. Como adicionar um novo cliente, a oportunidade de editar (só o seu nome), remover o próprio cliente do arquivo, entre outras funcionalidades. Já na parte do estoque, onde acontece verdadeiramente a mágica de qualquer supermercado, só foi possível implementar o cadastro de um novo produto (podendo colocar seu nome, a quantidade desse produto que será colocada, o seu valor monetário por unidade e a data em que ele foi cadastrado no sistema), listar os produtos que estão contidos no estoque e removê-los. Na parte do sistema, os principais métodos foram aqueles relacionados a manipulação do arquivo, em sua abertura, fechamento, salvar as informações que o usuário fez no sistema para com o arquivo também. Abaixo podemos ver a lista de métodos que foram adicionados:</p>
            <ul>
                <h4>Cliente</h4>
                <li><b>void</b> cadastrarClientes()</li>
                <li><b>void</b> editarCliente()</li>
                <li><b>void</b> listarClientes()</li>
                <li><b>void</b> removerCliente()</li>
                <li><b>void</b> buscarClientesNome()</li>
                <h4>Produto</h4>
                <li><b>void</b> cadastrarProduto()</li>
                <li><b>void</b> listarProdutos()</li>
                <li><b>void</b> removerProduto()</li>
                <h4>Manipulação de Arquivo</h4>
                <li><b>void</b> carregarClientes()</li>
                <li><b>void</b> carregarEstoque()</li>
                <li><b>void</b> salvarAlteracoes()</li>
            </ul>
            <li><h3>Compilação</h3></li>
            <p>Para ser realizada a compilação do programa foi feito um make, tornando a compilação mais fácil. Uma observação bastante interessante é que os passos que serão descritos são especificamente para sistemas UNIX, dessa forma para outras plataformas como MacOs ou Windows o processo pode ser diferente.</p>
            <ul>
                <li>Abra o terminal do seu sistema.</li>
                <li>Direcione o local do seu terminal para a pasta do projeto</li>
                <li>Já está com o terminal na pasta raiz do projeto, digite <b>make</b> (aguarde a compilação)</li>
                <li>Após a compilação escreva <b>./bin/Run</b> e aperter enter.</li>
            </ul>
    </ul>